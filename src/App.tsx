import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import routes from './config/routes';
import SimpleSidebar from './components/base/Navbar';

function App() {
    return (
        <>
            <SimpleSidebar>
                <BrowserRouter>
                    <Routes>
                        {routes.map((route, index) =>
                            <Route key={index} path={route.path} element={<route.element/>}/>)}
                    </Routes>
                </BrowserRouter>
            </SimpleSidebar>
        </>
    );
}

export default App;
