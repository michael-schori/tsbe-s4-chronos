import Home from '../components/Home';
import {IconType} from 'react-icons';
import {FiHome} from 'react-icons/fi';

interface IRoute {
    icon: IconType;
    name: string;
    path: string;
    element: any;
    protected: boolean;
}

const routes: IRoute[] = [
    {
        icon: FiHome,
        name: 'Home',
        path: '/',
        element: Home,
        protected: false
    }
];

export default routes;
